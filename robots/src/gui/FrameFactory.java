package gui;

public class FrameFactory {

    public static StatefulFrame create(String frameName) {
        try {
            Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass(frameName);
            return (StatefulFrame) clazz.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }
}
