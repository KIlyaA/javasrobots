package gui;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ApplicationStore {

    private final static String PATH = System.getProperty("user.home") + "/robots_state.bin";

    public static void saveState(List<WindowState> state) {
        try(ObjectOutputStream oss =
                    new ObjectOutputStream(new FileOutputStream(PATH))) {

            oss.writeObject(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<WindowState> getState() {
        List<WindowState> state = new ArrayList<>();
        try(ObjectInputStream ois =
                    new ObjectInputStream(new FileInputStream(PATH))) {
            state = (List<WindowState>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return state;
    }
}
