package gui;

import javax.swing.*;
import java.awt.*;

public abstract class StatefulFrame extends JInternalFrame {
    public StatefulFrame(String s, boolean b, boolean b1, boolean b2, boolean b3) {super(s, b, b1, b2, b3);}

    public  void restoreState(WindowState state) {
        this.setSize(state.getWidth(), state.getHeight());
        this.setLocation(state.getX(), state.getY());

        this.invalidate();
    }

    public WindowState saveState() {
        Point location = this.getLocation();

        WindowState state = new WindowState();
        state.setX(location.x);
        state.setY(location.y);
        state.setWidth(this.getWidth());
        state.setHeight(this.getHeight());

        return state;
    }

    public abstract WindowState getInitialState();
}
