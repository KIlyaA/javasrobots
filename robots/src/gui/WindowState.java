package gui;

import java.io.Serializable;
import java.util.List;

public class WindowState implements Serializable{
    private String name;

    private int x;
    private int y;

    private int width;
    private int height;

    private List<WindowState> children;

    public WindowState() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<WindowState> getChildren() {
        return children;
    }

    public void setChildren(List<WindowState> children) {
        this.children = children;
    }
}
