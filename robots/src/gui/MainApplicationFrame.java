package gui;

import log.Logger;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class MainApplicationFrame extends JFrame {

    private final JDesktopPane desktopPane = new JDesktopPane();
    private ArrayList<StatefulFrame> children = new ArrayList<>();

    public MainApplicationFrame() {
        int inset = 50;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(inset, inset,
                screenSize.width - inset * 2,
                screenSize.height - inset * 2);

        setContentPane(desktopPane);
        setJMenuBar(createMenuBar());

        List<WindowState> state = ApplicationStore.getState();
        if(state.isEmpty()) {
            GameWindow gameWindow = new GameWindow();
            gameWindow.restoreState(gameWindow.getInitialState());

            LogWindow logWindow = new LogWindow();
            logWindow.restoreState(logWindow.getInitialState());

            addWindow(gameWindow);
            addWindow(logWindow);
        } else {
            for (WindowState windowState : state) {
                StatefulFrame statefulFrame = FrameFactory.create(windowState.getName());
                statefulFrame.restoreState(windowState);

                addWindow(statefulFrame);
            }
        }

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                super.windowClosing(windowEvent);
                Object[] options = { "Рыбку съесть", "Нахуй сесть" };

                int option = JOptionPane.showOptionDialog(
                        windowEvent.getWindow(),
                        "Вы уверены, что хотите выйти?",
                        "Подверждение выхода",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);

                if(option == 0) {
                    List<WindowState> state = MainApplicationFrame.this.children
                            .stream().map(StatefulFrame::saveState).collect(Collectors.toList());

                    ApplicationStore.saveState(state);

                    windowEvent.getWindow().setVisible(false);
                    windowEvent.getWindow().dispose();
                    System.exit(0);
                }
            }
        });
    }

    private void addWindow(StatefulFrame frame) {
        frame.addInternalFrameListener(new InternalFrameAdapter() {
            @Override
            public void internalFrameClosing(InternalFrameEvent internalFrameEvent) {
                super.internalFrameClosing(internalFrameEvent);
                MainApplicationFrame.this.children.remove(internalFrameEvent.getInternalFrame());
            }
        });

        this.children.add(frame);
        desktopPane.add(frame);
        frame.setVisible(true);
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(createControlMenu());
        menuBar.add(createLookAndFeelMenu());
        menuBar.add(createTestsMenu());

        return menuBar;
    }

    private JMenu createControlMenu() {
        JMenu menu = new JMenu("Программа");
        menu.setMnemonic(KeyEvent.VK_D);

        JMenuItem menuItem = new JMenuItem("Новое окно протокола");
        menuItem.setMnemonic(KeyEvent.VK_N);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.addActionListener(actionEvent -> {
            StatefulFrame frame = FrameFactory.create(LogWindow.class.getName());
            frame.restoreState(frame.getInitialState());
            addWindow(frame);
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Новое игровое окно");
        menuItem.setMnemonic(KeyEvent.VK_N);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.addActionListener(actionEvent -> {
            StatefulFrame frame = FrameFactory.create(GameWindow.class.getName());
            frame.restoreState(frame.getInitialState());
            addWindow(frame);
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Выход");
        menuItem.setMnemonic(KeyEvent.VK_Q);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("quit");
        menuItem.addActionListener(actionEvent -> {
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(
                    new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });
        menu.add(menuItem);

        return menu;
    }

    private JMenu createLookAndFeelMenu() {
        JMenu menu = new JMenu("Режим отображения");
        menu.setMnemonic(KeyEvent.VK_V);
        menu.getAccessibleContext().setAccessibleDescription(
                "Управление режимом отображения приложения");

        JMenuItem menuItem = new JMenuItem("Системная схема", KeyEvent.VK_S);
        menuItem.addActionListener((event) -> {
            setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            this.invalidate();
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Универсальная схема", KeyEvent.VK_S);
        menuItem.addActionListener((event) -> {
            setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            this.invalidate();
        });
        menu.add(menuItem);

        return menu;
    }

    private JMenu createTestsMenu() {
        JMenu menu = new JMenu("Тесты");
        menu.setMnemonic(KeyEvent.VK_T);
        menu.getAccessibleContext().setAccessibleDescription("Тестовые команды");

        JMenuItem menuItem = new JMenuItem("Сообщение в лог", KeyEvent.VK_S);
        menuItem.addActionListener((event) -> {
            Logger.debug("Новая строка");
        });
        menu.add(menuItem);

        return menu;
    }

    private void setLookAndFeel(String className) {
        try {
            UIManager.setLookAndFeel(className);
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e) {
            // just ignore
        }
    }
}
