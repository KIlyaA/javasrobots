package gui;

import java.awt.*;

import javax.swing.JPanel;

public class GameWindow extends StatefulFrame
{
    private final GameVisualizer m_visualizer;
    public GameWindow() 
    {
        super("Игровое поле", true, true, true, true);
        m_visualizer = new GameVisualizer();
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(m_visualizer, BorderLayout.CENTER);
        getContentPane().add(panel);
        pack();
    }

    @Override
    public WindowState saveState() {
        WindowState state = super.saveState();
        state.setName(this.getClass().getName());

        return state;
    }

    @Override
    public WindowState getInitialState() {
        WindowState state = new WindowState();
        state.setName(this.getClass().getName());
        state.setWidth(400);
        state.setHeight(400);

        return state;
    }
}
