package gui;

import java.awt.*;

import javax.swing.*;

import log.LogChangeListener;
import log.LogEntry;
import log.LogWindowSource;
import log.Logger;

public class LogWindow extends StatefulFrame implements LogChangeListener
{
    private LogWindowSource m_logSource = Logger.getDefaultLogSource();
    private TextArea m_logContent;

    public LogWindow()
    {
        super("Протокол работы", true, true, true, true);
        m_logSource.registerListener(this);
        m_logContent = new TextArea("");
        m_logContent.setSize(200, 500);
        
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(m_logContent, BorderLayout.CENTER);
        getContentPane().add(panel);
        pack();
        updateLogContent();
    }

    private void updateLogContent() {
        StringBuilder content = new StringBuilder();
        for (LogEntry entry : m_logSource.all()) {
            content.append(entry.getMessage()).append("\n");
        }
        m_logContent.setText(content.toString());
        m_logContent.invalidate();
    }

    @Override
    public void onLogChanged()
    {
        EventQueue.invokeLater(this::updateLogContent);
    }

    @Override
    public WindowState saveState() {
        WindowState state = super.saveState();
        state.setName(this.getClass().getName());

        return state;
    }

    @Override
    public WindowState getInitialState() {
        WindowState state = new WindowState();
        state.setName(this.getClass().getName());
        state.setWidth(300);
        state.setHeight(800);

        return state;
    }
}
